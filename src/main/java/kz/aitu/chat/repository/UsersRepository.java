package kz.aitu.chat.repository;


import kz.aitu.chat.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<Users, Long> {

}
