package kz.aitu.chat.controller;

import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Participant;
import kz.aitu.chat.repository.ChatRepository;
import kz.aitu.chat.repository.ParticipantRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("api/v1/chat")
@AllArgsConstructor
public class ChatController {
    private ChatRepository ChatRepository;
    private ParticipantRepository participantRepository;
    private ParticipantRepository participantRepository1;


    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(ChatRepository.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(ChatRepository.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<?> addChat(@RequestBody Chat Chat){

        return ResponseEntity.ok(ChatRepository.save(Chat));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteChat(@PathVariable Long id){
        ChatRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
    @GetMapping("/find/{userId}")
    public ResponseEntity<?> findUserChats(@PathVariable Long userId){
        List<Participant> participants = participantRepository.findAllByUserId(userId);
        List<Chat> chatList = new ArrayList<>();
        for ( Participant participant : participants){
            Optional<Chat> chat = ChatRepository.findById(participant.getChatId());
            chatList.add(chat.get());
        }


        return ResponseEntity.ok(chatList);
    }

}
