package kz.aitu.chat.controller;

import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/users")
@AllArgsConstructor
public class UsersController {
    private UsersRepository UsersRepository;

    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(UsersRepository.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(UsersRepository.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<?> addUsers(@RequestBody Users users){

        return ResponseEntity.ok(UsersRepository.save(users));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUsers(@PathVariable Long id){
        UsersRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
