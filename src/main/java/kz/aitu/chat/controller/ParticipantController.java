package kz.aitu.chat.controller;

import kz.aitu.chat.model.Message;
import kz.aitu.chat.repository.ParticipantRepository;
import kz.aitu.chat.model.Participant;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/participants")
@AllArgsConstructor
public class ParticipantController {
    private ParticipantRepository ParticipantRepository;

    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(ParticipantRepository.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(ParticipantRepository.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<?> addParticipant(@RequestBody Participant participant){

        return ResponseEntity.ok(ParticipantRepository.save(participant));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUsers(@PathVariable Long id){
        ParticipantRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
