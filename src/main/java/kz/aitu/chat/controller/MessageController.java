package kz.aitu.chat.controller;

import kz.aitu.chat.repository.MessageRepository;
import kz.aitu.chat.model.Message;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/message")
@AllArgsConstructor
public class MessageController {
    private MessageRepository MessageRepository;

    @GetMapping("")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(MessageRepository.findAll());
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(MessageRepository.findById(id));
    }
    @PostMapping("")
    public ResponseEntity<?> addMessage(@RequestBody Message message){

        return ResponseEntity.ok(MessageRepository.save(message));
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUsers(@PathVariable Long id){
        MessageRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
